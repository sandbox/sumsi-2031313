<?php

/**
 * @file
 * Contains Drupal\Core\ParamConverter\ParamConverterManager.
 */

namespace Drupal\Core\ParamConverter;

use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Cmf\Component\Routing\Enhancer\RouteEnhancerInterface;
use Symfony\Cmf\Component\Routing\RouteObjectInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;

use Drupal\Core\ParamConverter\ParamConverterInterface;

/**
 * Provides a service which allows to enhance (say alter) the arguments coming
 * from the URL.
 *
 * A typical use case for this would be upcasting a node id to a node entity.
 *
 * This class will not enhance any of the arguments itself, but allow other
 * services to register to do so.
 */
class ParamConverterManager implements RouteEnhancerInterface {

  /**
   * Converters managed by the ParamConverterManager.
   *
   * @var array
   */
  protected $converters;

  /**
   * Converters managed by the ParamConverterManager.
   *
   * @var array
   */
  protected $sortedConverters;

  /**
   * Adds a converter to the paramconverter service.
   *
   * @see \Drupal\Core\DependencyInjection\Compiler\RegisterParamConvertersPass
   *
   * @param \Drupal\Core\ParamConverter\ParamConverterInterface $converter
   *   The converter to add.
   */
  public function addConverter(ParamConverterInterface $converter, $priority = 0) {
    if (empty($this->converters[$priority])) {
      $this->converters[$priority] = array();
    }
    $this->converters[$priority][] = $converter;
    unset($this->sortedConverters);
    return $this;
  }

  /**
   * Sorts the converters and flattens them.
   *
   * @return array
   *   The sorted parameter converters.
   */
   protected function getConverters() {
    if (!isset($this->sortedResolvers)) {
      krsort($this->converters);
      $this->sortedConverters = array();
      foreach ($this->converters as $converters) {
        $this->sortedConverters = array_merge($this->sortedConverters, $converters);
      }
    }
    return $this->sortedConverters;
  }

  /**
   * Implements \Symfony\Cmf\Component\Routing\Enhancer\ŖouteEnhancerIterface.
   *
   * Iterates over all registered converters and allows them to alter the
   * defaults.
   *
   * @param array $defaults
   *   The getRouteDefaults array.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return array
   *   The modified defaults.
   */
  public function enhance(array $defaults, Request $request) {
    $route = $defaults[RouteObjectInterface::ROUTE_OBJECT];

    // Iterate over all registered converters to upcast request attributes.
    foreach ($this->getConverters() as $converter) {
      if (!$converted = $converter->convert($defaults, $route)) {
        continue;
      }

      foreach ($converted as $key => $value) {
        // Throw a 404 if an attribute has been converted to NULL.
        if ($value === NULL) {
          throw new NotFoundHttpException();
        }

        if (!isset($defaults["_converted_$key"])) {
          $defaults["_converted_$key"] = $value;
        }
      }
    }

    return $defaults;
  }
}
