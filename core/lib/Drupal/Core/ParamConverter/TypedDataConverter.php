<?php

/**
 * @file
 * Contains Drupal\Core\ParamConverter\TypedDataConverter.
 */

namespace Drupal\Core\ParamConverter;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;
use Drupal\Core\TypedData\TypedDataManager;

/**
 * Upcasts request attributes to typed data objects.
 */
class TypedDataConverter implements ParamConverterInterface {

  /**
   * The typed data manager.
   *
   * @var \Drupal\Core\TypedData\TypedDataManager
   */
  protected $typedDataManager;

  /**
   * Constructs a new TypedDataConverter object.
   *
   * @param \Drupal\Core\TypedData\TypedDataManager $typed_data_manager
   *   The typed data manager.
   */
  public function __construct(TypedDataManager $typed_data_manager) {
    $this->typedDataManager = $typed_data_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function convert(array $variables, Route $route) {
    if (!$definitions = $route->getOption('typeddata') ?: array()) {
      return;
    }

    // Iterate over all variables and upcast upcast those for which there are
    // typed data definitions in the route options.
    $converted = array();
    foreach ($variables as $key => $value) {
      // Don't touch already converted parameters.
      if (isset($variables["_converted_$key"]) || !isset($definitions[$key])) {
        continue;
      }

      // Load the typed data object via the typed data manager.
      $converted[$key] = $this->typedDataManager->load($value, $definitions[$key]);
    }
    return $converted;
  }
}
