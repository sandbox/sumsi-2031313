<?php

/**
 * @file
 * Contains \Drupal\Core\Controller\ControllerResolver.
 */

namespace Drupal\Core\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ControllerResolver as BaseControllerResolver;
use Symfony\Component\HttpKernel\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * ControllerResolver to enhance controllers beyond Symfony's basic handling.
 *
 * It adds two behaviors:
 *
 *  - When creating a new object-based controller that implements
 *    ContainerAwareInterface, inject the container into it. While not always
 *    necessary, that allows a controller to vary the services it needs at
 *    runtime.
 *
 *  - By default, a controller name follows the class::method notation. This
 *    class adds the possibility to use a service from the container as a
 *    controller by using a service:method notation (Symfony uses the same
 *    convention).
 */
class ControllerResolver extends BaseControllerResolver {

  /**
   * The injection container that should be injected into all controllers.
   *
   * @var Symfony\Component\DependencyInjection\ContainerInterface
   */
  protected $container;

  /**
   * Constructs a new ControllerResolver.
   *
   * @param Symfony\Component\DependencyInjection\ContainerInterface $container
   *   A ContainerInterface instance.
   * @param Symfony\Component\HttpKernel\Log\LoggerInterface $logger
   *   (optional) A LoggerInterface instance.
   */
  public function __construct(ContainerInterface $container, LoggerInterface $logger = NULL) {
    $this->container = $container;

    parent::__construct($logger);
  }

  /**
   * Returns a callable for the given controller.
   *
   * @param string $controller
   *   A Controller string.
   *
   * @return mixed
   *   A PHP callable.
   *
   * @throws \LogicException
   *   If the controller cannot be parsed
   *
   * @throws \InvalidArgumentException
   *   If the controller class does not exist
   */
  protected function createController($controller) {
    // Controller in the service:method notation.
    $count = substr_count($controller, ':');
    if ($count == 1) {
      list($service, $method) = explode(':', $controller, 2);
      return array($this->container->get($service), $method);
    }

    // Controller in the class::method notation.
    if (strpos($controller, '::') !== FALSE) {
      list($class, $method) = explode('::', $controller, 2);
      if (!class_exists($class)) {
        throw new \InvalidArgumentException(sprintf('Class "%s" does not exist.', $class));
      }
      // @todo Remove the second in_array() once that interface has been removed.
      if (in_array('Drupal\Core\Controller\ControllerInterface', class_implements($class))) {
        $controller = $class::create($this->container);
      }
      else {
        $controller = new $class();
      }
    }
    else {
      throw new \LogicException(sprintf('Unable to parse the controller name "%s".', $controller));
    }

    if ($controller instanceof ContainerAwareInterface) {
      $controller->setContainer($this->container);
    }

    return array($controller, $method);
  }

  /**
   * {@inheritdoc}
   *
   * Change the way how the controller resolver work, by just passing in the
   * upcasted values, if there is a type hinting.
   */
  protected function doGetArguments(Request $request, $controller, array $parameters) {
    $attributes = $request->attributes->all();
    $arguments = array();
    foreach ($parameters as $parameter) {
      if (array_key_exists($parameter->name, $attributes)) {
        if ($parameter->getClass()) {
          $arguments[] = $attributes['_converted_' . $parameter->name];
        }
        else {
          $arguments[] = $attributes[$parameter->name];
        }
      }
      elseif ($parameter->getClass() && $parameter->getClass()->isInstance($request)) {
        $arguments[] = $request;
      }
      elseif ($parameter->isDefaultValueAvailable()) {
        $arguments[] = $parameter->getDefaultValue();
      }
      else {
        if (is_array($controller)) {
          $controller = sprintf('%s::%s()', get_class($controller[0]), $controller[1]);
        }
        elseif (is_object($controller)) {
          $controller = get_class($controller);
        }
        throw new \RuntimeException(sprintf('Controller "%s" requires that you provide a value for the "$%s" argument (because there is no default value or because there is a non optional argument after this one).', $controller, $parameter->name));
      }
    }

    return $arguments;
  }

}
