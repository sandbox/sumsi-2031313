<?php

/**
 * @file
 * Contains LoadableInterface.
 */

namespace Drupal\Core\TypedData;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Interface for loadable typed data objects.
 */
interface LoadableInterface {

  /**
   * Loads a typed data object.
   *
   * @param mixed $id
   *   The unique identifier of the typed data object to be loaded.
   * @param array $definition
   *   The typed data definition.
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container this object should use.
   *
   * @return \Drupal\Core\TypedData\TypedDataInterface|null
   *   The loaded typed data object or NULL if it could not be loaded.
   */
  public static function load($id, array $definition, ContainerInterface $container);

}
