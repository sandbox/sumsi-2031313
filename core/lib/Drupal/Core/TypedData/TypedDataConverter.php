<?php

/**
 * @file
 * Contains Drupal\Core\TypedData\TypedDataConverter.
 */

namespace Drupal\Core\TypedData;

use Drupal\Core\ParamConverter\ParamConverterInterface;
use Symfony\Component\Routing\Route;

/**
 * Class TypedDataConverter
 * @package Drupal\Core\TypedData
 */
class TypedDataConverter implements ParamConverterInterface {

  /**
   * {@inheritdoc}
   */
  public function process(array &$variables, Route $route, array &$converted) {
    $parameters = $route->getOption('parameters') ?: array();
    foreach ($parameters as $parameter => $options) {

    }
  }

}
