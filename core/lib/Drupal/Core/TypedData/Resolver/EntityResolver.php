<?php

/**
 * @file
 * Contains Drupal\Core\TypedData\Resolver\EntityResolver.
 */

namespace Drupal\Core\TypedData\Resolver;

use Drupal\Core\TypedData\TypedDataManager;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Generates typed data definitions for specific entity routes.
 */
class EntityResolver implements ResolverInterface {

  /**
   * The typed data manager.
   *
   * @var \Drupal\Core\TypedData\TypedDataManager;
   */
  protected $typedDataManager;

  /**
   * Constructs a EntityResolver object.
   *
   * @param TypedDataManager $typed_data_manager
   *   The typed data manager to use.
   */
  public function __construct(TypedDataManager $typed_data_manager) {
    $this->typedDataManager = $typed_data_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function resolveParameterTypes(Route $route) {
    $defaults = $route->getDefaults();
    if (!empty($defaults['_controller'])) {
      return;
    }

    // Either the '_entity_form' or '_entity_list' have to be set.
    if (empty($defaults['_entity_form']) && empty($defaults['_entity_list'])) {
      return;
    }

    // This typed data resolver is only capable of generating typed data
    // definitions for routes that have either '_entity_form' or '_entity_list'
    // in the route defaults array.
    $entity_type = NULL;
    if (!empty($defaults['_entity_form'])) {
      list ($entity_type) = explode('.', $defaults['_entity_form']);
    }
    elseif (!empty($defaults['_entity_list'])) {
      $entity_type = $defaults['_entity_list'];
    }

    $definition[$entity_type] = $this->typedDataManager->getDefinition('entity');
    $definition[$entity_type]['constraints']['EntityType'] = $entity_type;
    return $definition;
  }

}
