<?php

/**
 * @file
 * Contains Drupal\Core\TypedData\Resolver\ResolverManager.
 */

namespace Drupal\Core\TypedData\Resolver;

use Drupal\Core\TypedData\TypedDataManager;
use ReflectionMethod;
use Symfony\Component\Routing\RouteCollection;

/**
 * Generates typed data definitions for route parameters.
 */
class ResolverManager {

  /**
   * An array of registered typed data resolvers.
   *
   * @var array
   */
  protected $resolvers = array();

  /**
   * Array of registered typed resolvers sorted by their priority.
   *
   * @var array
   */
  protected $sortedResolvers;

  /**
   * Registers a typed data resolver with the manager.
   *
   * @param \Drupal\Core\TypedData\Resolver\ResolverInterface $resolver
   *   The typed data resolver to register.
   * @param int $priority
   *   (optional) The priority of the typed data resolver. Defaults to 0.
   *
   * @return \Drupal\Core\TypedData\Resolver\ResolverManager
   *   The called object for chaining.
   */
  public function addTypeDataResolver(ResolverInterface $resolver, $priority = 0) {
    if (empty($this->resolvers[$priority])) {
      $this->resolvers[$priority] = array();
    }
    $this->resolvers[$priority][] = $resolver;
    unset($this->sortedResolvers);
    return $this;
  }

  /**
   * Sorts the resolvers and flattens them.
   *
   * @return array
   *   The sorted typed data resolvers.
   */
  public function getTypedDataResolvers() {
    if (!isset($this->sortedResolvers)) {
      krsort($this->resolvers);
      $this->sortedResolvers = array();
      foreach ($this->resolvers as $resolvers) {
        $this->sortedResolvers = array_merge($this->sortedResolvers, $resolvers);
      }
    }
    return $this->sortedResolvers;
  }

  /**
   * Tries to generate typed data definitions for each route in a collection.
   *
   * @param RouteCollection $routes
   *   A route collection.
   */
  public function resolveParameterTypes(RouteCollection $routes) {
    $resolvers = $this->getTypedDataResolvers();
    foreach ($routes->all() as $route) {
      $definitions = array();
      foreach ($resolvers as $resolver) {
        if ($parameters = $resolver->resolveParameterTypes($route)) {
          $definitions = array_merge($parameters, $definitions);
        }
      }

      // Merge the resolved typed data definitions into the route options.
      if (!empty($definitions)) {
        // Do not override manually defined typed data definitions.
        $definitions = array_merge($definitions, ($route->getOption('typeddata') ?: array()));
        $route->setOption('typeddata', $definitions);
      }
    }
  }

}
