<?php

/**
 * @file
 * Contains Drupal\Core\TypedData\Resolver\ReflectionResolver.
 */

namespace Drupal\Core\TypedData\Resolver;

use Drupal\Core\TypedData\TypedDataManager;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Generates typed data definitions based on a controller's type hints.
 */
class ReflectionResolver implements ResolverInterface {

  /**
   * The typed data manager.
   *
   * @var \Drupal\Core\TypedData\TypedDataManager;
   */
  protected $typedDataManager;

  /**
   * Constructs a ReflectionResolver object.
   *
   * @param TypedDataManager $typed_data_manager
   *   The typed data manager to use.
      }
   */
  public function __construct(TypedDataManager $typed_data_manager) {
    $this->typedDataManager = $typed_data_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function resolveParameterTypes(Route $route) {
    $defaults = $route->getDefaults();

    // Iterate over all entries in the defaults array to find any callable
    // that might have type hints from which we can draw clues.
    $definitions = array();
    foreach ($defaults as $default) {
      if (is_string($default) && strpos($default, '::') !== FALSE) {
        list($class, $method) = explode('::', $default, 2);
        if (!class_exists($class)) {
          throw new \InvalidArgumentException(sprintf('Class "%s" does not exist.', $class));
        }

        $callable = array($class, $method);
        if ($parameters = $this->getParameterTypes($callable)) {
          $definition = array_merge($parameters, $definitions);
        }
      }
    }

    return $definitions;
  }

  /**
   * Tries to determine the typed data types via the type hints on a callable.
   *
   * @param mixed $callable
   *   A callable.
   *
   * @return array
   *   An array of typed data definitions keyed by parameter name.
   */
  protected function getParameterTypes($callable) {
    if (is_array($callable)) {
      $reflection = new \ReflectionMethod($callable[0], $callable[1]);
    }
    elseif (is_object($callable) && !$callable instanceof \Closure) {
      $reflection = new \ReflectionObject($callable);
      $reflection = $reflection->getMethod('__invoke');
    }
    else {
      $reflection = new \ReflectionFunction($callable);
    }

    $definitions = array();
    foreach ($reflection->getParameters() as $parameter) {
      $definitions[$parameter->getName()] = $this->getParameterType($parameter);
    }
    return $definitions;
  }

  /**
   * Tries to determine the typed data type of a given parameter.
   *
   * @param \ReflectionParameter $parameter
   *   The parameter to generate a typed data definition for.
   *
   * @return array
   *   The typed data definition for the given parameter.
   */
  protected function getParameterType(\ReflectionParameter $parameter) {
    if (!$class = $parameter->getClass()) {
      // The parameter does not have a proper type hint.
      return;
    }

    // Ignore request object type hints.
    $name = $class->getName();
    $request = 'Symfony\Component\HttpFoundation\Request';
    if ($name == $request || $class->isSubclassOf($request)) {
      return;
    }

    // Try to find a typed data type that matches the type hint.
    foreach ($this->typedDataManager->getDefinitions() as $type => $definition) {
      if ($definition['class'] == $class) {
        // Return the first match.
        return $definition;
      }
    }
  }

}
