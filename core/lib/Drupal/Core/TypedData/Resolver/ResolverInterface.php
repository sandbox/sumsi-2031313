<?php

/**
 * @file
 * Contains \Drupal\Core\TypedData\Resolver\ResolverInterface.
 */

namespace Drupal\Core\TypedData\Resolver;

use Symfony\Component\Routing\Route;

/**
 * Interface for typed data definition resolvers.
 */
interface ResolverInterface {

  /**
   * Tries to generate typed data definitions for the parameters of a route.
   *
   * @param Route $route
   *   The route object for which to generate typed data definitions.
   *
   * @return array
   *   An array of typed data definitions keyed by parameter name.
   */
  public function resolveParameterTypes(Route $route);

}
