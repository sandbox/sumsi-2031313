<?php

/**
 * @file
 * Contains Drupal\Core\DependencyInjection\Compiler\RegisterTypedDataResolversPass.
 */

namespace Drupal\Core\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Registers typed data resolver services with the typed data resolver manager.
 */
class RegisterTypedDataResolversPass implements CompilerPassInterface {

  /**
   * Register typed data resolver services with the typed data resolver manager.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerBuilder $container
   *   The container to process.
   */
  public function process(ContainerBuilder $container) {
    if (!$container->hasDefinition('typed_data_resolver_manager')) {
      return;
    }

    $manager = $container->getDefinition('typed_data_resolver_manager');
    foreach ($container->findTaggedServiceIds('typed_data_resolver') as $id => $attributes) {
      $priority = isset($attributes[0]['priority']) ? $attributes[0]['priority'] : 0;
      $manager->addMethodCall('addTypeDataResolver', array(new Reference($id), $priority));
    }
  }
}
