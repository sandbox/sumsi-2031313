<?php

/**
 * @file
 * Contains Drupal\Core\EventSubscriber\TypedDataSubscriber.
 */

namespace Drupal\Core\EventSubscriber;

use Drupal\Core\TypedData\Resolver\ResolverManager;
use Drupal\Core\TypedData\TypedDataDetector;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Routing\RoutingEvents;
use Drupal\Core\Routing\RouteBuildEvent;

/**
 * Typed data subscriber for route build events.
 */
class TypedDataSubscriber implements EventSubscriberInterface {

  /**
   * The typed data resolver.
   *
   * @var \Drupal\Core\TypedData\Resolver\ResolverManager
   */
  protected $resolverManager;

  /**
   * Constructs a TypedDataSubscriber object.
   */
  public function __construct(ResolverManager $resolver_manager) {
    $this->resolverManager = $resolver_manager;
  }

  /**
   * Generates typed data definition for route arguments.
   *
   * @param \Drupal\Core\Routing\RouteBuildEvent $event
   *   The event to process.
   */
  public function onRoutingRouteAlterSetAccessCheck(RouteBuildEvent $event) {
    $this->resolverManager->resolveParameterTypes($event->getRouteCollection());
  }

  /**
   * Registers the methods in this class that should be listeners.
   *
   * @return array
   *   An array of event listener definitions.
   */
  static function getSubscribedEvents() {
    // Setting very low priority to ensure typed data discovery runs after
    $events[RoutingEvents::ALTER][] = array('onRoutingRouteAlterSetAccessCheck', 0);

    return $events;
  }
}
